const mongodb = require('mongodb');
const client = mongodb.MongoClient;
const uri = "mongodb://mongo:27017";
const assert = require('assert'); 

const testDb = "test";
const devDb = "dev";
const prodDb = "prod";

const dbName = testDb;

module.exports = {
    get: function get(id, collectionName){
    return new Promise((resolve, reject) => {
      client.connect(uri, function(err, client) {
        if (err != null) {
          reject(err);
        }

        const db = client.db(dbName);
        db.collection(collectionName).findOne({id: id}).then(data => {
          client.close(false);
          resolve(data)
        }).catch(err => {
          client.close(false);
          reject(err)
        });

      });
    })
  },

  add: function add(message, collectionName){
    return new Promise((resolve, reject) => { client.connect(uri, function(err, client) {
      if (err != null) {
        reject(err);
      }

        const db = client.db(dbName);
        
        db.collection(collectionName).insertOne(message, function(err, result){
          if (err != null) {
            reject(err);
          }

          assert.equal(1, result.insertedCount);

          client.close(false);
          resolve(message.id);
        });
      });
    });
  },

  update: function update(message, collectionName){
    return new Promise ((resolve, reject) => {
      client.connect(uri, function(err, client) {
        if (err != null) {
          reject(err);
        }

        const db = client.db(dbName);
        db.collection(collectionName).updateOne({ id: message.id }, {$set: message});

        client.close(false);
        resolve(message.id);
      });
    });
  },

  delete: function del (id, collectionName){
    return new Promise((resolve, reject) => {
      client.connect(uri, function(err, client) {
        if (err != null) {
          reject(err);
        }

        const db = client.db(dbName);
        db.collection(collectionName).deleteOne({ id: id });

        client.close(false);
        resolve(true);
      });
    });
  },

  //collection names
  messagesCollectionName: "messages"
}