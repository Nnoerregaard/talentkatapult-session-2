const express = require('express');
const bodyParser = require('body-parser');
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');
const { makeExecutableSchema } = require('graphql-tools');

const mongoDBUtility = require('./utilities/mongoDB');

// The GraphQL schema in string form
const typeDefs = `
  type Message {
    id: ID!,
    content: String!,
    author: String,
    timestamp: Int
  }
  input MessageInput {
    id: ID!,
    content: String,
    author: String,
    timestamp: Int
  }

  type Query { 
    message(id: ID!): Message
  }
  type Mutation { 
    addMessage(message: MessageInput!): ID,
    updateMessage(message: MessageInput!): ID,
    deleteMessage(id: ID!): Boolean
  }
`;

// The resolvers
const resolvers = {
  Query: { 
    message: (root, args, context) => {
      return mongoDBUtility.get(args.id, mongoDBUtility.messagesCollectionName);
    },
  },
  Mutation: { 
    addMessage: (_, args) => {
      const message = {"id": args.message.id, "content": args.message.content, "author": args.message.author, "timestamp": args.message.timestamp};      
      return mongoDBUtility.add(message, mongoDBUtility.messagesCollectionName);
    },

    updateMessage: (_, args) => {
      return mongoDBUtility.update(args.message, mongoDBUtility.messagesCollectionName);
    },

    deleteMessage: (_, args) => {
      return mongoDBUtility.delete(args.id, mongoDBUtility.messagesCollectionName); 
    },
  },
};

// Put together a schema
const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

// Initialize the app
const app = express();

// The GraphQL endpoint
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));

// GraphiQL, a visual editor for queries
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

// Start the server
app.listen(3000, () => {
  console.log('Go to http://localhost:3000/graphiql to run queries!');
});
