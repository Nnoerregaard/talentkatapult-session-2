FROM node

ENV PORT 3000

EXPOSE 3000

COPY package.json package.json
RUN npm install

RUN npm install -g nodemon
RUN npm install -g typescript

COPY . /

RUN tsc

CMD ["npm", "start"]